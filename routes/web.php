<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('gethomedata', 'HomeController@gethomedata');

# Blog routes

Route::get('getposts', 'BlogController@getBlog');
Route::get('getpost/{slug}', 'BlogController@show');
Route::get('getrandompost', 'BlogController@getRandomEntries');

# Categories routes
Route::get('getcategoryposts/{slug}', 'CategoriesController@getCategoryPosts');
Route::get('getcategories', 'CategoriesController@getCategories');
# send email rout
Route::post('sendemail', 'SendemailController@send');

# voyager routes
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('sitemap', function(){
  
  // create new sitemap object
  $sitemap = App::make("sitemap");
  
  // add items to the sitemap (url, date, priority, freq)
  $sitemap->add(URL::to('/'), Carbon\Carbon::now(), '1.0', 'daily');
  $sitemap->add(URL::to('/blog'), Carbon\Carbon::now(), '1.0', 'daily');
  
  // get all posts from db
  $posts = DB::table('posts')->orderBy('created_at', 'desc')->get();
  
  // add every post to the sitemap
  foreach ($posts as $post)
  {
    $sitemap->add(URL::to('/blog/'.$post->slug), $post->updated_at, '0.9', 'daily');
  }
  
  // generate your sitemap (format, filename)
  $sitemap->store('xml', 'sitemap');
  // this will generate file mysitemap.xml to your public folder
  
});
Route::get('/hasloggedin', function() {
  if(Auth::check()) {
    return response('true');
  }
});

Route::get('/lang/{locale?}', function($locale = null) {
    if ($locale == 'es')
        session()->put('locale', $locale);

    if ($locale == 'en' || $locale == null)
        session()->put('locale', $locale);

    Cache::forget('lang.js');
    return redirect()->back();
});

Route::get('/js/lang.js', function () {
    $strings = Cache::rememberForever('lang.js', function () {
        $lang = config('app.locale');

        $files   = glob(resource_path('lang/' . $lang . '/*.php'));
        $strings = [];

        foreach ($files as $file) {
            $name           = basename($file, '.php');
            $strings[$name] = require $file;
        }

        return $strings;
    });

    header('Content-Type: text/javascript');
    echo('window.i18n = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');

Route::get('/{any?}', 'HomeController@index');
Route::get('/{any?}/{slug}', 'HomeController@index');

