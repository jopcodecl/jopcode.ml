let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'public/css/app.css',
    'public/css/bootstrap.min.css',
    'public/css/fontawesome.css',
    'public/css/ionicons.css',
    'public/css/pikaday.css',
    'public/css/animate.css',
    'public/css/theme.css',
    'public/css/custom.css'
], 'public/css/all.css');



