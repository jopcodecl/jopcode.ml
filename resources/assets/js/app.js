
require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import Meta from 'vue-meta'
import NProgress from 'nprogress'
import VueDisqus from 'vue-disqus'
import Lodash from 'lodash'

import App from './App'
import Home from './components/Home'
import Blog from './components/Blog'
import Servicios from './components/Servicios'
import Contacto from './components/Contacto'
import ShowPost from './components/ShowPost'
import ShowCategory from './components/ShowCategory'

import '../../../node_modules/nprogress/nprogress.css'
import BootstrapVue from 'bootstrap-vue'
//import { Carousel } from 'bootstrap-vue/es/components';

//Vue.use(Carousel);
Vue.use(BootstrapVue)
Vue.use(VueDisqus)
Vue.use(VueRouter)
Vue.use(Meta)


Vue.component('Contact', require('./components/Contact.vue'));

Vue.mixin({
    methods: {
        setHeaderTags (data) {
            return [
                document.title = 'JopCode ~ '+data[0].title,
                document.getElementsByTagName('meta')[name='searchtitle'].content = data[0].title,
                document.getElementsByTagName('meta')[name='description'].content = this.strip_html_tags(data[0].body),
                document.getElementsByTagName('meta')[name='keywords'].content += ' '+data[0].meta_keywords,
                ]
        },
        strip_html_tags(str) {
            if ((str===null) || (str===''))
                return false;
            else
                str = str.toString();
            return str.replace(/<[^>]*>/g, '');
        }
    }
});


const routes = [
    /*{
      name: 'Home',
      path: '/',
      component: Home,
    },*/
    {
        name: 'ShowPost',
        path: '/blog/:slug',
        component: ShowPost
    },
    {
        name: 'ShowCategory',
        path: '/categoria/:slug',
        component: ShowCategory
    },
    {
        name: 'Contacto',
        path: '/contacto',
        component: Contacto
    },
    {
        name: 'Servicios',
        path: '/servicios',
        component: Blog
    },
    {
        name: 'Inicio',
        path: '/',
        component: Blog
    },
    {
        path: '*',
        component: Blog
    }
];


Vue.prototype.trans = (string) => Lodash.get(window.i18n, string);

const router = new VueRouter({ mode: 'history', routes: routes });
NProgress.configure({
    template: '<div class="loading-overlay" role="bar"><span class="spinner"></span></div>'
})
router.beforeResolve((to, from, next) => {
    if (to.name) {
        NProgress.start();
    }
    next()
  })
  

new Vue(Vue.util.extend({ router }, App)).$mount('#app');