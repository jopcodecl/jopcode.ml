<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <style type="text/css">
            body, b, p, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
            body{
                height: 100% !important; 
                margin: 0 !important; 
                padding: 0 !important; 
                width: 100% !important;
                background: #ffffff;
                color: #353434;
            }
        </style>
    </head>
    <body>
            <b>{!! $name !!}</b> te ha enviado una nueva cotizacion, Telefono: <b>{!! $phone !!}</b>, Email:<b>{!! $from !!}</b> y con el mensaje de:
            
            <br>
            <p>
                {!! $content !!}
            </p>
    </body>
</html>
