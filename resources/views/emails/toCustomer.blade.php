<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <style type="text/css">
            body, b, p, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
            body{
                height: 100% !important; 
                margin: 0 !important; 
                padding: 0 !important; 
                width: 100% !important;
                background: #ffffff;
                color: #353434;
            }
        </style>
    </head>
    <body>
            Hola <b>{!! $name !!}</b> soy jonathan de <a href="http://jopcode.ml">jopcode</a> gracias por enviar su cotizacion, le responderemos lo antes posible.
            si tiene alguna consulta no dude en escribirnos por WhatsApp al: +56972454362 o bien via llamada telefonica al : +56954415674.

            Nuestros mas cordiales saludos, hasta pronto.
    </body>
</html>
