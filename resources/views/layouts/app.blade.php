<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NVKC4HS');</script>
    <!-- End Google Tag Manager -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta http-equiv="Cache-Control" content="max-age=3600">
  <title>JopCode ~ Páginas Web Económicas ~ Sitios web económicos</title>
  <meta name="description" content="Diseño de páginas web económicas. Diseño Web Responsive GRATIS en todos nuestros planes, Páginas web económicas, Diseño de pagina web, Sitios web Económicos, Creacion de Páginas web"
  />
  <meta name="keywords" content="paginas web economicas, paginas web economicas, paginas web economicas, sitio web economico, sitios web economicos, Diseño paginas web baratas, Creacion de Páginas web, paginas web osorno, puerto montt, chile, chile sur, creacion de paginas web osorno, se crean paginas web osorno, osorno chile"
  />
  <meta name="searchtitle" content="Paginas Web Economicas - Sitios web economicos - Diseño web economico - sitio web economico - Diseño paginas web baratas, Creacion de Páginas web"
  />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ asset('css/all.css') }}">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <link rel="stylesheet" href="{{ asset("vendor/tcg/voyager/assets/js/plugins/codesample/css/prism.css") }}">

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NVKC4HS"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
    <!--[if lt IE 8]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->
    <div id="app"></div>
    <!-- include jQuery and Popper.js (both required for Bootstrap 4) and the theme JS file -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
      crossorigin="anonymous"></script>
    <script src="/js/lang.js"></script>
    <script src="{{ asset('js/theme.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    
</body>
</html>