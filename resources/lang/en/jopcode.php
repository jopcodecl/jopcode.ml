<?php
return [
    'blog' => [
        'title' => 'Offencive Security',
        'subtitle' => 'I Just Enjoyed The Challenge',
        'date' => 'Date',
        'by' => 'By'
    ],
    'menu' => [
    	'home' => 'Home'
    ],
    'lang' => 'Language'
];