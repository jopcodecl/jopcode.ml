<?php
return [
    'blog' => [
        'title' => 'Seguridad Ofensiva',
        'subtitle' => 'Solo Disfruto de los Retos',
        'date' => 'Fecha',
        'by' => 'Por'
    ],
    'menu' => [
    	'home' => 'Inicio'
    ],
    'lang' => 'Idioma'
];