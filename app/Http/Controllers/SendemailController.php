<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendemailController extends Controller
{
    public function send(Request $request)
    {
        $this->validate($request, [
            'from' => 'required|email',
            'name' => 'required',
            'phone' => 'required',
            'message' => 'required'
        ]);
        $data = array(
            'from' => $request->from,
            'name' => $request->name,
            'phone' => $request->phone,
            'content' => $request->message,
        );

        Mail::send('emails.toMe', $data, function ($message) {
            $message->from('cotizacion@jopcode.ml', 'jopcode');
            $message->to('jopcodecl@gmail.com')->subject('Nueva cotizacion en JopCode.ml');
        });
        return "done";
    }
}
