<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoriesController extends Controller
{
    public function getCategories() {
        $categories = Category::all();
        return $categories;
    }
    public function getCategoryPosts($slug){
        $category = Category::where('slug',$slug)->with('posts')->get();
        return $category;
    }
}
