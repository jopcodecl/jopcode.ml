<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class BlogController extends Controller
{
    private $lang;
    public function __construct() {
        $this->lang = session('locale');
    }

    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function index()
    {
      return view('blog');
    }
    public function getBlog()
    {
        
        $postsTranslated = collect();
        foreach(Post::with('author')->get()->translate($this->lang) as $post) {
            $postsTranslated->push([
                'id' => $post->id,
                'title' => $post->title,
                'image' => $post->image,
                'slug' => $post->slug,
                'author' => $post->author,
                'author_id' => $post->author_id,
                'category_id' => $post->category_id,
                'created_at' => $post->created_at,

            ]);
        }

        return $this->paginate($postsTranslated, 5);
    }

    public function show($slug) {
        #return Post::where('slug',$slug)->with('author')->get();
        $posts = Post::where('slug',$slug)->with('author')->get()->translate($this->lang);
        $postTranslated = collect();
        foreach ($posts as $post) {
            $postTranslated->push([
                "id" => $post->id,
                "author_id" => $post->author_id,
                "category_id" => $post->category_id,
                "title" => $post->title,
                "seo_title" => $post->seo_title,
                "excerpt" => $post->excerpt,
                "body" => $post->body,
                "image" => $post->image,
                "slug" => $post->slug,
                "meta_description" => $post->meta_description,
                "meta_keywords" => $post->meta_keywords,
                "status" => $post->status,
                "featured" => $post->featured,
                "created_at" => $post->created_at,
                "updated_at" => $post->updated_at,
                "author" => $post->author
            ]);
        }

        return $postTranslated;
    }
    public function getRandomEntries() {
        $post = Post::inRandomOrder()->with('author','category')->limit(4)->get();
        return $post;
    }
}
















