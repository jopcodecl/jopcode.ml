<?php

namespace App\Http\Controllers;

use App\OurJob;
use App\Pricing;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index($any = null)
    {
      return view('layouts.app');
    }
    public function getHomeData()
    {
        $pricings = Pricing::all();
        $ourjobs = OurJob::all();
        return compact(['pricings', 'ourjobs']);
    }
    
}
