<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Post extends Model
{
    use Translatable;
    protected $guarded = [];
    protected $translatable = ['title','body','excerpt'];
    
    public function getImageAttribute()
    {
      return asset('storage/'.$this->attributes['image']);
    }
    
  public function author()
    {
        return $this->belongsTo('App\User', 'author_id', 'id');
    }
  public function category() {
    return $this->belongsTo('App\Category','category_id', 'id');
  }
}
