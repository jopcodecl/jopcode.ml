<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Post::class, function (Faker $faker) {
    return [
      'author_id' => '1',
      'title' => $faker->sentence(),
      'body' => $faker->text(),
      'image'=> $faker->imageUrl(600,800, 'cats'),
      'slug' => $faker->unique()->text(20)
      
    ];
});
